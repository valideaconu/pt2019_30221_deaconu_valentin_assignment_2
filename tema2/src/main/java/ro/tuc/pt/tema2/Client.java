package ro.tuc.pt.tema2;

import java.util.Random;

public class Client {
	private static int AUTO_INCREMENT = 0;
	private int id;
	private Service service;
	private int arrivingTime;
	private int waitingTime;
	
	private int imageId;
	
	public Client(Service service, int arrivingTime) {
		this.id = AUTO_INCREMENT++;
		this.imageId = (new Random()).nextInt((4 - 1) + 1) + 1;
		this.setService(service);
		this.setArrivingTime(arrivingTime);
	}
	
	public Client copy() {
		Client n = new Client(this.service, this.arrivingTime);
		n.waitingTime = this.waitingTime;
		n.id = this.id;
		
		return n;
	}

	public int getId() {
		return id;
	}
	
	public int getImageId() {
		return imageId;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public int getArrivingTime() {
		return arrivingTime;
	}

	public void setArrivingTime(int arrivingTime) {
		this.arrivingTime = arrivingTime;
	}

	public int getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}

	@Override
	public String toString() {
		return new String("Client #" + id + ", arrive time: " + arrivingTime + ", service time: " + service.getRequiredTime());
	}
}
