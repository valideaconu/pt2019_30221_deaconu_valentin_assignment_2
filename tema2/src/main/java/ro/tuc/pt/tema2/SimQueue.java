package ro.tuc.pt.tema2;

import java.security.InvalidParameterException;
import java.util.ArrayList;

public class SimQueue implements Runnable {	
	private final int id;
	private ClientGenerator clientGenerator;
	private int currentTime;
	private int finishTime;
	private double avgWaitingTime;
	private double avgEmptyTime;
	private double avgServiceTime;
	
	public ArrayList<Client> clients;
	
	private App window;
	
	public SimQueue(App window, int id, int minArrivingTime, int maxArrivingTime, int minServiceTime, 
			int maxServiceTime, int clientsNumber, int finishTime) throws InvalidParameterException {
		if (minArrivingTime >= maxArrivingTime)
			throw new InvalidParameterException("Minimum arriving time has to be lower than maximum arriving time.");
		
		if (minServiceTime >= maxServiceTime)
			throw new InvalidParameterException("Minimum service time has to be lower than maximum service time.");
		
		if (clientsNumber < 0)
			throw new InvalidParameterException("Clients number must be positive.");
		
		if (finishTime < 0)
			throw new InvalidParameterException("Finishing time must be positive");
		
		this.id = id;
		this.clientGenerator = new ClientGenerator(minArrivingTime, maxArrivingTime, minServiceTime, maxServiceTime);
		this.finishTime = finishTime;
		
		this.window = window;
		
		this.currentTime = 0;
		this.clientGenerator.generateClients(clientsNumber);
		
		this.clients = new ArrayList<Client>();
		this.clients.addAll(this.clientGenerator.clients);
	}
	
	public void run() {
		int clientsIndex = 0;
		int waitFor = 0;
		Client client = null;
		for (currentTime = 0; currentTime <= finishTime; ++currentTime) {
			if (waitFor == 0) {
				if (clientsIndex >= clientGenerator.clients.size())
					break;
				
				if (client != null)
					clients.remove(client);
				
				client = clientGenerator.clients.get(clientsIndex);
				if (currentTime >= client.getArrivingTime()) {
					waitFor = client.getService().getRequiredTime();
					avgServiceTime += waitFor;
					client.setWaitingTime(currentTime - client.getArrivingTime());
					++clientsIndex;
					window.writeLog(id, "[" + this.toString() + ", @" + currentTime + "m] Serving client no. " + client.toString() + ".");
				} else {
					window.writeLog(id, "[" + this.toString() + ", @" + currentTime + "m] Waiting for customers.");
					++avgEmptyTime;
				}
			} else if(waitFor > 0) {
				window.writeLog(id, "[" + this.toString() + ", @" + currentTime + "m] Busy for " + waitFor + " more minute(s).");
				--waitFor;
			}
			
			window.announceFinished(id, currentTime, SimulationState.RUNNING);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) { }	
		}
		computeResults();
	}
	
	public double getAverageWatingTime() {
		return avgWaitingTime;
	}
	
	public double getAverageServiceTime() {
		return avgServiceTime;
	}
	
	public double getAverageEmptyTime() {
		return avgEmptyTime;
	}
	
	public void computeResults() {
		for (Client c : clientGenerator.clients)
			avgWaitingTime += c.getWaitingTime();
		avgWaitingTime /= clientGenerator.clients.size();
		
		avgEmptyTime /= finishTime;
		
		avgServiceTime /= clientGenerator.clients.size();
		
		window.writeResultsLog(id, "[" + this.toString() + "] Average waiting time: " + avgWaitingTime + " minutes.\n" +
					"[" + this.toString() + "] Average empty time: " + avgEmptyTime + " minutes.\n" +
					"[" + this.toString() + "] Average service time: " + avgServiceTime + " minutes.\n");

		window.announceFinished(id, currentTime, SimulationState.FINISHED);
	}
	
	public String toString() {
		return new String("Q#" + id);
	}

}
