package ro.tuc.pt.tema2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimulationListener implements ActionListener {
	private App window;
	
	public SimulationListener(App window) {
		this.window = window;
	}
	
	public void actionPerformed(ActionEvent e) {
		window.updateInterface(SimulationState.STARTING);
	}

}
