package ro.tuc.pt.tema2;

public enum SimulationState {
	WAITING, STARTING, RUNNING, FINISHED
}
