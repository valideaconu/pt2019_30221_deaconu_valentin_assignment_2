package ro.tuc.pt.tema2;

import java.util.ArrayList;
import java.util.Random;

public class ClientGenerator {
	public ArrayList<Client> clients;
	private int minAt;
	private int maxAt;
	private int minSt;
	private int maxSt;
	
	public ClientGenerator(int minArrivingTime, int maxArrivingTime, int minServiceTime, int maxServiceTime) {
		clients = new ArrayList<Client>();
		
		minAt = minArrivingTime;
		maxAt = maxArrivingTime;
		minSt = minServiceTime;
		maxSt = maxServiceTime;
	}
	
	public void generateClient() {
		Service service = new Service(minSt, maxSt);
		// get last client's arriving time
		int lastArrivingTime = (clients.size() > 0) ? clients.get(clients.size() - 1).getArrivingTime() : 0;
		// generate a random number in [minAt, maxAt] and add to it last arriving time
		int arrivingTime = (new Random()).nextInt((maxAt - minAt) + 1) + minAt + lastArrivingTime;
		Client client = new Client(service, arrivingTime);
		clients.add(client);
	}
	
	public void generateClients(int clientsNumber) {
		for (int i = 0; i < clientsNumber; ++i) {
			generateClient();
		}
	}
}
