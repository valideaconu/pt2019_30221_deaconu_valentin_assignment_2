package ro.tuc.pt.tema2;

import java.util.Random;

public class Service {
	private static int AUTO_INCREMENT = 0;
	private final int id;
	private int requiredTime;
	
	public Service(int requiredTime) {
		this.id = AUTO_INCREMENT++;
		this.requiredTime = requiredTime;
	}
	
	public Service(int minServiceTime, int maxServiceTime) {
		this((new Random()).nextInt((maxServiceTime - minServiceTime) + 1) + minServiceTime);
	}
	
	public int getId() {
		return id;
	}
	
	public int getRequiredTime() {
		return requiredTime;
	}

	public void setRequiredTime(int requiredTime) {
		this.requiredTime = requiredTime;
	}

	@Override
	public String toString() {
		return "Service [id=" + id + ", requiredTime=" + requiredTime + "]";
	}
}
