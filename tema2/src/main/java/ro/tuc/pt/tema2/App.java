package ro.tuc.pt.tema2;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.security.InvalidParameterException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultCaret;

public class App extends JFrame {
	private static final long serialVersionUID = 2483834324732242621L;
	public static final Font FONT_USED = new Font("Ubuntu", Font.PLAIN, 16);
	
	private JSpinner arrTimeMin;
	private JSpinner arrTimeMax;
	private JSpinner srvTimeMin;
	private JSpinner srvTimeMax;
	private JSpinner queueSp;
	private JSpinner clientsSp;
	private JSpinner finishSp;
	
	private JTextArea eventLogTa;
	private String[] queueLogs;
	
	private JPanel simulationMenuView;
	private JSplitPane simSp;
	
	private SimulationState state;
	
	private Thread[] queueThread;
	private SimQueue[] simQueues;
	private boolean[] sleeping;
	
	public int currentTime;
	private JLabel eventTitleLbl;
	
	private int maximumArrivedClients;
	private int peakTime;
	
    public static void main( String[] args ) {
    	App window = new App();
    	window.setVisible(true);
    }
    
    // App constructor
    public App() {
    	// mainframe properties
    	this.setTitle("Supermarket queue simulator");
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	this.setSize(1600, 900);
    	this.setLocationRelativeTo(null);
    	this.state = SimulationState.WAITING;
    	this.currentTime = 0;
    	
    	// divide simulationMenu
    	JPanel simulationMenuOptions = simulationOptionPanel();
    	simulationMenuView = simulationViewPanel();
    	
    	simSp = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    	simSp.setResizeWeight(0.15);
    	simSp.setEnabled(false);
    	simSp.setDividerSize(0);
    	
    	simSp.add(simulationMenuOptions);
    	simSp.add(simulationMenuView);
    	
    	// divide eventlog
    	JPanel eventLogTitle = new JPanel();
    	eventLogTitle.setLayout(new GridBagLayout());
    	eventTitleLbl = new JLabel("Event Log", SwingConstants.CENTER);
    	eventTitleLbl.setFont(FONT_USED);
    	eventLogTitle.add(eventTitleLbl);
    	
    	eventLogTa = new JTextArea();
    	eventLogTa.setFont(FONT_USED);
    	eventLogTa.setLineWrap(true);
    	eventLogTa.setWrapStyleWord(true);
    	DefaultCaret caret = (DefaultCaret)eventLogTa.getCaret();
    	caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    	eventLogTa.setText("No logs yet");
    	
    	JScrollPane eventLogSp = new JScrollPane(eventLogTa);
    	
    	JSplitPane eventSp = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    	eventSp.setResizeWeight(0.10);
    	eventSp.setEnabled(false);
    	eventSp.setDividerSize(0);
    	
    	eventSp.add(eventLogTitle);
    	eventSp.add(eventLogSp);
    	
    	// divide frame
    	JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    	sp.setResizeWeight(0.75);
    	sp.setEnabled(false);
    	sp.setDividerSize(0);
    	
    	sp.add(simSp);
    	sp.add(eventSp);
    	
    	this.add(sp);
    }

    // Method were thread can communicate with the application
    public void announceFinished(int id, int time, SimulationState state) {   
    	sleeping[id] = true;
    	
    	boolean allSleeping = true;
    	for (int i = 0; i < queueThread.length; ++i)
    		if (!sleeping[i])
    			allSleeping = false;
    	
    	if (allSleeping) {
    		this.currentTime = time;
    		
    		if (state == SimulationState.RUNNING) {
    			updateInterface(SimulationState.RUNNING);
	    		for (int i = 0; i < queueThread.length; ++i)
	    			sleeping[i] = false;
    		} else if (state == SimulationState.FINISHED) {
    			updateInterface(SimulationState.FINISHED);
    		}
    	}
    }
    
    // Method for updating interface
	public void updateInterface(SimulationState state) {
		this.state = state;

		String united = new String(eventLogTa.getText());
		if (queueThread != null)
			for (int i = 0; i < queueThread.length; ++i)
				united += (queueLogs[i] == null ? "" : queueLogs[i]);
		
		eventLogTa.setText(united);
		eventTitleLbl.setText("Event Log | Time left: " + (((Integer)finishSp.getValue()) - currentTime) + " minutes");
		
		simulationMenuView.removeAll();
		simSp.remove(simulationMenuView);
		
		simSp.setBottomComponent(simulationViewPanel());

		simSp.repaint();
		simSp.revalidate();
		
		this.repaint();
		this.revalidate();
	}
	
	// Method for threads to write in eventlog
	public void writeLog(int id, String logText) {
		queueLogs[id] = (logText + "\n");
	}
	
	public void writeResultsLog(int id, String logText) {
		queueLogs[id] = ("\n" + logText + "\n");
	}
    
	// Panel for options
    private JPanel simulationOptionPanel() {
    	JPanel panel = new JPanel();
    	
    	panel.setLayout(new GridLayout(2, 5));
    	
    	JLabel arriveTimeLbl = new JLabel("Arriving Time", SwingConstants.CENTER);
    	arriveTimeLbl.setToolTipText("Insert minimum and maximum value for arriving time (in minutes) between customers.");
    	JLabel serviceTimeLbl = new JLabel("Service Time", SwingConstants.CENTER);
    	serviceTimeLbl.setToolTipText("Insert minimum and maximum time value (in minutes) for a service.");
    	JLabel queuesNumberLbl = new JLabel("Queues no.", SwingConstants.CENTER);
    	queuesNumberLbl.setToolTipText("Insert number of queues, between 1 and 5.");
    	JLabel clientsNumberLbl = new JLabel("Clients no.", SwingConstants.CENTER);
    	clientsNumberLbl.setToolTipText("Insert number of customers at the supermarket.");
    	JLabel finishTimeLbl = new JLabel("Simulation finish after", SwingConstants.CENTER);
    	finishTimeLbl.setToolTipText("Insert time value (in minutes) when simulation will finish.");
    	
    	panel.add(arriveTimeLbl);
    	panel.add(serviceTimeLbl);
    	panel.add(queuesNumberLbl);
    	panel.add(clientsNumberLbl);
    	panel.add(finishTimeLbl);
    	
    	arrTimeMin = new JSpinner(new SpinnerNumberModel(5, 0, 15, 1));
    	arrTimeMax = new JSpinner(new SpinnerNumberModel(10, 0, 15, 1));
    	JSplitPane atsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    	atsp.setDividerSize(0);
    	atsp.setEnabled(false);
    	atsp.setResizeWeight(0.5);
    	atsp.add(arrTimeMin);
    	atsp.add(arrTimeMax);
    	arriveTimeLbl.setLabelFor(atsp);
    	panel.add(atsp);

    	srvTimeMin = new JSpinner(new SpinnerNumberModel(5, 0, 15, 1));
    	srvTimeMax = new JSpinner(new SpinnerNumberModel(10, 0, 15, 1));
    	JSplitPane stsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    	stsp.setDividerSize(0);
    	stsp.setEnabled(false);
    	stsp.setResizeWeight(0.5);
    	stsp.add(srvTimeMin);
    	stsp.add(srvTimeMax);
    	serviceTimeLbl.setLabelFor(stsp);
    	panel.add(stsp);
    	
    	queueSp = new JSpinner(new SpinnerNumberModel(3, 1, 5, 1));
    	queuesNumberLbl.setLabelFor(queueSp);
    	panel.add(queueSp);
    	
    	clientsSp = new JSpinner(new SpinnerNumberModel(50, 1, 100, 1));
    	clientsNumberLbl.setLabelFor(clientsSp);
    	panel.add(clientsSp);
    	
    	finishSp = new JSpinner(new SpinnerNumberModel(30, 1, 60, 1));
    	finishTimeLbl.setLabelFor(finishSp);
    	panel.add(finishSp);
    	
    	for (Component c : panel.getComponents()) {
    		if (c instanceof JSplitPane) {
    			((JSplitPane) c).getLeftComponent().setFont(FONT_USED);
    			((JSplitPane) c).getRightComponent().setFont(FONT_USED);
    		} else
    			((JComponent) c).setFont(FONT_USED);
    	}
    	
    	return panel;
    }
    
    private JPanel simulationViewPanel() {
    	JPanel panel = new JPanel();
    	
    	if (state == SimulationState.WAITING) {
    		// if application is in waiting state draw start button
    		panel.setLayout(new GridBagLayout());
    		
    		JButton startSimulation = new JButton("START");
    		startSimulation.setFont(FONT_USED);
    		startSimulation.setHorizontalAlignment(SwingConstants.CENTER);
    		startSimulation.addActionListener(new SimulationListener(this));
    		startSimulation.setPreferredSize(new Dimension(200, 70));
    		
    		panel.add(startSimulation);
    	} else if(state == SimulationState.STARTING) {
    		// if application is in starting state (pressed START)
    		eventLogTa.setText(new String(""));
    		
    		// deactivate all spinners and extract values
    		int minArrivingTime = (Integer)arrTimeMin.getValue();
    		arrTimeMin.setEnabled(false);
    		int maxArrivingTime = (Integer)arrTimeMax.getValue();
    		arrTimeMax.setEnabled(false);
    		int minServiceTime = (Integer)srvTimeMin.getValue();
    		srvTimeMin.setEnabled(false);
			int maxServiceTime = (Integer)srvTimeMax.getValue();
    		srvTimeMax.setEnabled(false);
    		int queuesNumber = (Integer)queueSp.getValue();
    		queueSp.setEnabled(false);
    		int clientsNumber = (Integer)clientsSp.getValue();
    		clientsSp.setEnabled(false);    		
			int finishingTime = (Integer)finishSp.getValue();
    		finishSp.setEnabled(false);

    		// compute clients number
    		int q = clientsNumber / queuesNumber;
    		int r = clientsNumber % queuesNumber;
    		
    		int columnsNumber = q + r;
    		
    		// distribute clients in queues
    		int[] clientsOverQueue = new int[queuesNumber];
    		for (int i = 0; i < queuesNumber; ++i) {
    			clientsOverQueue[i] = q + r;
    			r = (r > 0) ? (r - 1) : 0;
    		}
    		
    		// allocate queues
    		queueThread = new Thread[queuesNumber];
    		simQueues = new SimQueue[queuesNumber];
    		queueLogs = new String[queuesNumber];
    		sleeping = new boolean[queuesNumber];
    		
    		for (int i = 0; i < queuesNumber; ++i) {
    			queueLogs[i] = new String();
    			try {
    				simQueues[i] = new SimQueue(this, i, minArrivingTime, maxArrivingTime, minServiceTime, 
    						maxServiceTime,	clientsOverQueue[i], finishingTime);
    				queueThread[i] = new Thread(simQueues[i]);
    				r = (r > 0) ? (r - 1) : 0;
    			} catch (InvalidParameterException ex) {
    	    		writeLog(i, "Queue #" + i + " cannot be created: " + ex.getMessage());
    			}
    		}
    		
    		// draw queues
    		draw(panel, queuesNumber, columnsNumber);
    		
    		// start threads
    		for (int i = 0; i < queuesNumber; ++i) {
    			queueThread[i].start();
    		}
    		
    		state = SimulationState.RUNNING;
    	} else if (state == SimulationState.RUNNING) {
    		// if application is in running state (starting state finished)
    		int queuesNumber = (Integer)queueSp.getValue();
    		int clientsNumber = (Integer)clientsSp.getValue();

    		int q = clientsNumber / queuesNumber;
    		int r = clientsNumber % queuesNumber;
    		
    		int columnsNumber = q + r;
    		
    		// redraw interface
    		draw(panel, queuesNumber, columnsNumber);
    	} else if (state == SimulationState.FINISHED) {
    		// if application is in finished state (thread marked itself as finished)
    		boolean readyToGo = true;
    		for (int i = 0; i < queueThread.length; ++i) {
    			if (!sleeping[i]) {
    				readyToGo = false;
    			}
    		}
    		
    		// and all threads are done
    		if (readyToGo) {
    			try {
					Thread.sleep(1000);
				} catch (InterruptedException e) { }
    			// waiting for threads to finish
    			boolean test = false;
    			do {
    				test = true;
    				for (int i = 0; i < queueThread.length; ++i)
    					if (queueThread[i].isAlive() || !queueThread[i].isInterrupted())
    						test = false;
    			} while (test);
    			
    			// activate spinners
	    		arrTimeMin.setEnabled(true);
	    		arrTimeMax.setEnabled(true);
	    		srvTimeMin.setEnabled(true);
	    		srvTimeMax.setEnabled(true);
	    		queueSp.setEnabled(true);
	    		clientsSp.setEnabled(true);    		
	    		finishSp.setEnabled(true);
	
	    		panel.setLayout(new GridBagLayout());
	    		
	    		// draw start buton
	    		JButton startSimulation = new JButton("START AGAIN");
	    		startSimulation.setFont(FONT_USED);
	    		startSimulation.setHorizontalAlignment(SwingConstants.CENTER);
	    		startSimulation.addActionListener(new SimulationListener(this));
	    		
	    		panel.add(startSimulation);
	    		
	    		// get average results
	    		double avgWaitingTime = 0.0;
	    		double avgEmptyTime = 0.0;
	    		double avgServiceTime = 0.0;
	    		for (int i = 0; i < simQueues.length; ++i) {
	    			avgWaitingTime += simQueues[i].getAverageWatingTime();
	    			avgEmptyTime += simQueues[i].getAverageEmptyTime();
	    			avgServiceTime += simQueues[i].getAverageServiceTime();
	    		}
	    		
	    		avgWaitingTime /= simQueues.length;
	    		avgEmptyTime /= simQueues.length;
	    		avgServiceTime /= simQueues.length;
	    		
	    		// get overall results
	    		String results = new String( "Overall results:\n"
	    				+ "Average waiting time: " + avgWaitingTime + " minutes.\n"
	    				+ "Average empty time: " + avgEmptyTime + " minutes.\n"
	    				+ "Average service time: " + avgServiceTime + " minutes.\n"
	    				+ "Peak time: minute " + peakTime + ".\n"
	    				+ "For detalied results, please watch the event log.\n"
	    			);
	    		
	    		currentTime = 0;
	    		eventTitleLbl.setText("Event Log");
	    		eventLogTa.setText(eventLogTa.getText() + "\n" + results);
    		}
    	}
    	
    	return panel;
    }

    // Drawing method used to arrange customers in queues
	private void draw(JPanel panel, int queuesNumber, int columnsNumber) {
    	panel.setLayout(new GridLayout(queuesNumber, 1 + columnsNumber));
    	int arrivedClients = 0;
    	
		for (int i = 0; i < queuesNumber; ++i) {
			ImageIcon imageIcon = new ImageIcon("resources/cash_register.png"); 
			Image image = imageIcon.getImage();
			Image newimg = image.getScaledInstance(64, 64,  java.awt.Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(newimg);
			
			JLabel lbl = new JLabel(imageIcon);
			lbl.setToolTipText("Cash register #" + i);
			panel.add(lbl);
			
			for (int j = 0; j < columnsNumber; ++j) {
				if (j >= simQueues[i].clients.size() || simQueues[i].clients.get(j).getArrivingTime() > currentTime) {
					JLabel clbl = new JLabel(); 
					panel.add(clbl);
				} else {
					arrivedClients++;
					
					imageIcon = new ImageIcon("resources/customer" + simQueues[i].clients.get(j).getImageId() + ".png");
					image = imageIcon.getImage();
					newimg = image.getScaledInstance(64, 64,  java.awt.Image.SCALE_SMOOTH);
					imageIcon = new ImageIcon(newimg);
							
					JLabel clbl = new JLabel(imageIcon);
					clbl.setToolTipText(simQueues[i].clients.get(j).toString());
					panel.add(clbl);
				}
			}
		}
		
		if (arrivedClients > maximumArrivedClients) {
			maximumArrivedClients = arrivedClients;
			peakTime = currentTime;
		}
    }
}
